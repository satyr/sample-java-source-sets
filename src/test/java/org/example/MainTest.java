package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainTest {
    @Test
    public void testMain() {
        Main main = new Main();
        String env = main.getEnvironment();
        assertEquals("Test Environment", env);
    }
}
