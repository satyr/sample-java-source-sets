package org.example;

import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Main {
    private final String RESOURCE_NAME = "application.properties";
    private final Properties prop = new Properties();

    public static void main(String[] args) {
    }

    public Main() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try(InputStream resourceStream = loader.getResourceAsStream(RESOURCE_NAME)) {
            prop.load(resourceStream);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public String getEnvironment() {
        if (!this.prop.containsKey("name")) {
            throw new IllegalStateException("Missing the 'name' property");
        }
        return this.prop.getProperty("name");
    }
}
